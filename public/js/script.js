var headerPart = new Vue({
  el: 'header',
  methods: { 
  	methodLogin: function () { //for Login part
	    $('#right-form').toggleClass('visible');
  	},
    methodnav:function(){ //for navigation part
       $('.navigation-part ul').slideToggle(500);
    }
  },
  data: {
    navigation: [
      { title: 'HOME', url: '.'},
      { title: 'BECOME A PARTNER GYM', url: 'gymapplication' },
      { title: 'JOB', url: 'job' },
    ]
  }
})
var banner = new Vue({
    el:'.banner',
    data:{
      message:'Supercharge your existing gym membership' , 
      message1:'GYM ANYWHERE YOU WANT <br> WHENEVER YOU WANT',
      message2:'LEARN MORE',
    }
})
var howdoesworkheading = new Vue({
    el:'.how-work-heading',
    data:{
      message:'HOW DOES IT WORK' , 
      message1:'Gym Anywhere allows you to access any of our partner clubs, using your existing gym membership.'
    }
})
var howdoeswork = new Vue({  
  el: '#items',
  data: {
      items: [
        { title: 'JOINT IT', desc: 'Join us through your current gym or by joining one of our partner clubs.', icon: 'fa-pencil-square-o' },
        { title: 'BOOK IT', desc: 'Join us through your current gym or by joining one of our partner clubs.', icon: 'fa-mobile' },
        { title: 'WORK  IT', desc: 'Turn up to the gym and show your pass.', icon: 'fa-heart' },
      ]
    },
})
var gymowner = new Vue({
    el:'.gym-owner',
    data:{
      message:'GYM OWNER OR MANAGER?' , 
      message1:'Improve your clubs offering and create a new revenue stream <br /> by joining our rapidly growing network.',
      message2:'GET LISTED' 
    }
})
var joingymanywhere = new Vue({
    el:'.join-gymanywhere',
    data:{
      message:'JOIN GYMANYWHERE' , 
      message1:'Gain access to new members and create an exciting new revenue stream.'
    }
})
var gettouch = new Vue({
    el: '#social',
      data: {
        trs:[
          {icon:'fa-phone', desc:' +44 207 450 5529'},
          {icon:'fa-envelope', desc:' info@gymanywhere.co.uk'},
          {icon:'fa-twitter', desc:'@gym_anywhere'},
          {icon:'fa-facebook', desc:'facebook.com/gymanywhereuk'},
        ]
     }
})
var address = new Vue({
    el: '#address',
      data: {
        trs:[
          {icon:'fa-phone', desc:'Gym Anywhere Ltd <br> 1 Berkeley St <br> London, W1J 8DJ <br> UK'},
        ]
     }
})
var job = new Vue({
  el: '.contact-form',
  methods:{
    clickMe: function(){
     document.getElementById("contact-form").reset();
    }
  }
})

