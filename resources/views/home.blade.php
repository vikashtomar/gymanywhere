@extends('layout.mainlayout')

@section('content')	
<section class="banner-section">
	<div class="banner">
		 <div class="inner">
			<p>@{{message}}</p>
			<h2 class="major special" v-html="message1"></h2>                    
			<ul class="actions vertical">
				<li><a href="#learnmore" class="button big scrolly">@{{message2}}</a></li>
			</ul>
		</div>
	</div>
</section>	
<section class="how-does-work-section">
	<div class="how-does-work">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12">
					<div class="how-work-heading">
						<h1>@{{message}}</h1>
						<h3>@{{message1}}</h3>
					</div>
				</div>
			</div>
			<br><br><br>
			<div class="row" id="items">
				<div class="col-sm-4" v-for="item in items">
					<div class="how-blog-sec">
						<i class="fa" v-bind:class="[item.icon]" aria-hidden="true"></i>
						<h3>@{{item.title}}</h3>
						<p>@{{item.desc}}</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>	
<section class="gym-owner-section">
	<div class="gym-owner">
		<div class="comtainer">
			<div class="row">
				<div class="col-md-12">
					<h3>@{{ message }}</h3>
					<p v-html="message1"></p>
					<a href="#/" class="get-listed">@{{ message2 }}</a>
				</div>
			</div>
		</div>
	</div>
</section>
<section id="join-gymanywhere">
	<div class="join-gymanywhere">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="banner-text">
						<div class="dis-flex"> 
							<h1>@{{ message }}</h1>
							<h3>@{{ message1 }}</h3>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
@endsection