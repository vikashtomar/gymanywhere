<header>
	<div class="container-fluid">
		<div class="row">
			<div class="col-xs-4">
				<div class="logo">
					<a href=".">
						<h3><span>GYM</span>ANYWHERE</h3>
					</a>
				</div>
			</div>
			<div class="col-xs-8">
				<div class="header-navigation">
					<div class="navigation-part">
						<button type="button" v-on:click="methodnav()" class="fa fa-bars"></button>
						<ul>
							<li  v-for="nav in navigation">
								<a v-bind:href="nav.url">@{{nav.title}}</a>
							</li>
						</ul>
					</div>
					<div class="top-right-btn"  v-on:click="methodLogin()">
						<i class="fa fa-user"></i> Login 	
					</div>
					<div class="right-nav">
						<div id="right-form" >
							<h3>Sign in</h3>
							<ul class="signin-form">
							    <form id="myform">
							    	<label>Email address</label>
									<input type="text" class="text-boxes" placeholder="Email" name="email">
									<br><br>
									<label>Password</label>
									<input type="password" placeholder="Password" class="text-boxes" name="password">
									<br><br>
									<input type="submit" class="btn btn-primary logins" value="Login">
							    </form>
							</ul>
							<a href="#/" v-on:click="methodLogin()"  class="close"><i class="fa fa-close"></i></a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</header>



