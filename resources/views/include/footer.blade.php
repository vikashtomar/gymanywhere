<footer>
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-5 col-sm-12">
				<h3>GYM MEMBER LOOKING TO JOIN?</h3>
				<p>We are launching in summer 2017. Be the first to hear when we are live.</p>
				<form id="signup">
					<fieldset>
						<input type="text" placeholder="Email address" name="email">
					</fieldset>
					<fieldset>
						<input type="submit"  value="SIGN UP">
					</fieldset>
				</form>
			</div>	
			<div class="col-md-4 col-sm-6 col-xs-12">
				<h3>GET IN TOUCH</h3>
				<table class="footer-table" id="social">
					<tr v-for="tr in trs">
						<td><i class="fa" v-bind:class="[tr.icon]"></i></td>
						<td>@{{tr.desc}}</td>
					</tr>
				</table>
			</div>		
			<div class="col-md-3 col-sm-6 col-xs-12">
				<h3>OUR LOCATION</h3>
				<table class="footer-table" id="address">
					<tr v-for="tr in trs">
						<td><i class="fa"  v-bind:class="[tr.icon]"></i></td>
						<td  v-html="tr.desc"></td>
					</tr>
				</table>
			</div>		
		</div>
		<br><br><br>
		<div class="row">
			<div class="col-md-12 text-center">
				<p>&copy; Gym Anywhere Ltd. All rights reserved.</p>
			</div>
		</div>
	</div>
</footer>






