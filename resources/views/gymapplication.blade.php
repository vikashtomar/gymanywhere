@extends('layout.mainLayout')

@section('content')	

<section id="join-gymanywhere" class="martop50">
	<div class="join-gymanywhere">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="banner-text">
						<div class="dis-flex"> 
							<h1>JOIN GYMANYWHERE</h1>
							<h3>Gain access to new members and create an exciting new revenue stream.</h3>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<section id="contactSection">
	<div class="contact-sec">
		<div class="container">
			<div class="row">
				<div class="col-md-12">

					<h3>JOIN OUR RAPIDLY GROWING NETWORK</h3>
					<p>Gym members are increasingly demanding flexibility, whether it is with contracts, opening hours or membership plans. 63% say they would look for a gym that provides access to multiple locations.</p>
					<br><br>
					<h3>WHY GYMANYWHERE?</h3>
					<ul>
						<li>
							<strong>New revenue stream:</strong> Generate significant additional membership revenue.
						</li>
						<li>
							<strong>You have full control:</strong> You sell membership directly to your members as an add-on to their existing package.
						</li>
						<li>
							<strong>Free to join:</strong> We only get paid when you do, we simply take a share of the additional revenue you generate.
						</li>
						<li>
							<strong>Analytics and customer feedback:</strong> We provide you with all marketing material free of charge.
						</li>
						<li>
							<strong>Analytics:</strong> We will provide you with valuable customer feedback and market insight to help you continuously improve your offering.
						</li>
					</ul>
					<br><br>
					<h3>CONTACT US</h3>
					<p>Complete the form below or speak to one of our partner team on 020 3670 2325.</p>
					<br><br>
					<div class="contact-form">
						<form id="contact-form">
							<div class="row"> 
								<div class="col-sm-6">
									<fieldset>
										<input type="text" class="input-box" placeholder="Your Name" name="yourname">
									</fieldset>
								</div>
								<div class="col-sm-6">
									<fieldset>
										<input type="text" class="input-box" placeholder="Company Name" name="companyname">
									</fieldset>
								</div>
							</div>
							<div class="row"> 
								<div class="col-sm-6">
									<fieldset>
										<input type="email" class="input-box" placeholder="Email" name="email">
									</fieldset>
								</div>
								<div class="col-sm-6">
									<fieldset>
										<input type="text" class="input-box" placeholder="Telephone" name="telephone">
									</fieldset>
								</div>
							</div>
							<div class="row"> 
								<div class="col-sm-6">
									<fieldset>
										<select class="input-box" name="nosites">
											<option>-Number of Sites-</option>
											<option>1</option>
											<option>2</option>
											<option>3</option>
											<option>4</option>
											<option>5+</option>
										</select>
									</fieldset>
								</div>
								<div class="col-sm-6"></div>
							</div>
							<div class="row"> 
								<div class="col-sm-12">
									<fieldset>
										<textarea class="input-box" placeholder="Enter Your Message" name="message"></textarea>
									</fieldset>
								</div>
							</div>
							<div class="row"> 
								<div class="col-sm-12">
									<fieldset>
										<input type="submit" class="input-submit" value="Send Message">
										<input type="reset" class="input-submit reset" v-on:click="clickMe"  value="Reset">
									</fieldset>
								</div>
							</div>
						</form>

					</div>
				</div>
			</div>
		</div>
	</div>
</section>
@endsection