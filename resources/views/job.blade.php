@extends('layout.mainLayout')

@section('content')	
<section id="join-gymanywhere" class="martop50">
	<div class="join-gymanywhere">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="banner-text">
						<div class="dis-flex"> 
							<h1>JOBS</h1>
							<h3>Join our growing team</h3>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<section id="contactSection">
	<div class="contact-sec">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h3>CURRENT VACANCIES</h3>
					<br>
					<h5>BUSINESS DEVELOPMENT MANAGER</h5>
					<br><br>
					<p>Gym members are increasingly demanding flexibility, whether it is with contracts, opening hours or membership plans. 63% say they would look for a gym that provides access to multiple locations.</p>
					<br><br>
					<h3>WHY GYMANYWHERE?</h3>
					<ul>
						<li>
							<strong>Location :</strong> London, UK
						</li>
						<li>
							<strong>Salary :</strong> Competitive basic salary with very generous commission and bonus package
						</li>
						<li>
							<strong>Apply:</strong> Email your CV and covering letter to <a href="#/">jobs@gymanywhere.co.uk</a>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</section>
@endsection