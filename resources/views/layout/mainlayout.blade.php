<!DOCTYPE html>
<html>
  <head lang="en">
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1 user-scalable=no"/>
      <link rel="icon" type="image/png" href="{{ URL::asset('images/g.png') }}" />
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
      <link href="http://netdna.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" type="text/css" rel="stylesheet" />
      <link rel="stylesheet" href="{{ URL::asset('css/style.css') }}" type="text/css">
      <link rel="stylesheet" href="{{ URL::asset('css/responsive.css') }}" type="text/css">
      <title>Welcome To Gym Anywhere</title>
  </head>
  <body>
        @include ('include.header')
        @yield ('content')
        @include ('include.footer')
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="{{ URL::asset('js/jquery.validate.min.js') }}"></script>
        <script src="{{ URL::asset('js/smoothScroll.js') }}"></script>
        <script src="{{ URL::asset('js/vue.min.js') }}"></script>
        <script src="{{ URL::asset('js/script.js') }}"></script>
        <script type="text/javascript">
            $(document).ready(function () {
                $('#myform').validate({ // initialize the plugin
                    rules: {
                        email: {
                            required: true,
                            email: true
                        },
                        password: {
                            required: true,
                            minlength: 5
                        }
                    },
                    submitHandler: function (form) { 
                        alert('valid form submitted'); 
                        return false; 
                    }
                });
                $('#signup').validate({ 
                    rules: {
                        email: {
                            required: true,
                            email: true
                        },
                        password: {
                            required: true,
                            minlength: 5
                        }
                    },
                    submitHandler: function (form) { 
                        alert('valid form submitted');
                        return false; 
                    }
                });
                $('#contact-form').validate({ 
                    rules: {
                        email: {
                            required: true,
                            email: true
                        },
                        yourname: {
                            required: true,
                        },
                        companyname: {
                            required: true,
                        },
                        telephone: {
                            required: true,
                        },
                        nosites: {
                            required: true,
                            number: true
                        },
                        message: {
                            required: true,
                        }
                    },
                    submitHandler: function (form) { 
                        alert('valid form submitted');
                        return false; 
                    }
                });
            });
        </script>
  </body>
</html>
